package days

import "fmt"

type DayTwo struct {
	index []string
	//candidates []string
}

func (d DayTwo) Answer(test bool) {
	d.initIndex(test)
	d.solveFirst()
	d.solveSecond()
}

func (d *DayTwo) solveSecond() {
	fmt.Println("Answering second")
	i := 0
	max := len(d.index)
	for i = 0; i < max; i++ {
		base := d.index[i]
		for k := i + 1; k < max; k++ {
			if k >= max {
				panic("Index out of range. Tried to open index in candidates with K that was out of bounds")
			}
			match := d.stringDiff(base, d.index[k], 1)
			if match {
				sharedRunes := StringGetSharedChars(base, d.index[k])
				fmt.Printf("DayTwo - Second Answer: %s\n", string(sharedRunes))
				return
			}
		}
	}
}

func (d *DayTwo) solveFirst() {
	countTwo := 0
	countThree := 0
	for _, line := range d.index { //iterate lines
		m := make(map[rune]int)
		for _, char := range line { //insert runes/chars from the line into a map to count char occurrence
			m[char]++
		}
		twoVisisted := false
		threeVisisted := false
		for _, value := range m { //find out which char has 2 or more instances
			switch value {
			case 2:
				if twoVisisted {
					break
				}
				countTwo++
				twoVisisted = true
			case 3:
				if threeVisisted {
					break
				}
				countThree++
				threeVisisted = true
			}
		}
		// if twoVisisted || threeVisisted { //remember the lines since we need it for answer 2.
		// 	d.candidates = append(d.candidates, line)
		// }
	}
	fmt.Printf("DayTwo - First Answer: %d\n", d.generateChecksum(countTwo, countThree))
}

func (d *DayTwo) generateChecksum(countTwo int, countThree int) int {
	return countTwo * countThree
}

func (d *DayTwo) stringDiff(first string, second string, difference int) bool {
	diff := 0
	secondRunes := []rune(second)
	for i, rn := range first {
		if rn != secondRunes[i] {
			diff++
		}
	}
	if diff == difference {
		return true
	}
	return false
}

// Setup

func (d *DayTwo) initIndex(test bool) {
	d.index = make([]string, 0)
	ReadLines(getFilePath(2, test), d.createIndex)
}

func (d *DayTwo) createIndex(line string) error {
	d.index = append(d.index, line)
	return nil
}
