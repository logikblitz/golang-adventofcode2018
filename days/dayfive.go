package days

import (
	"io/ioutil"
	"log"
	"strings"

	sll "github.com/emirpasic/gods/lists/singlylinkedlist"
	_ "github.com/emirpasic/gods/utils"
)

type DayFive struct {
	//index *sll.List
	raw string
}

func (d DayFive) Answer(test bool) {
	d.initPuzzle(test)
	d.solveFirst()
	d.solveSecond()
}

func (d *DayFive) solveFirst() {
	log.Println("DayFive - Solve first puzzle")
	res := d.solveFirstPuzzle()
	log.Printf("DayFive - First Answer: %d\n", res)
}

func (d *DayFive) solveSecond() {
	log.Println("DayFive - Solve second puzzle")
	res := d.solveSecondPuzzle()
	log.Printf("DayFive - Second Answer: %d\n", res)
}

func (d *DayFive) solveFirstPuzzle() int {
	return d.optimizePolymer(d.raw)
}

const alphabet = "abcdefghijklmnopqrstuvwxyz"

func (d *DayFive) solveSecondPuzzle() int { //Really slow! :-()
	size := 100000
	for _, r := range alphabet {
		log.Printf("Char: %s\n", string(r))
		cs := swapCase(r)
		stripped := stripchars(d.raw, string(r)+string(cs))
		//trimmed := strings.Trim(stripped, " ")
		res := d.optimizePolymer(stripped)
		if res < size {
			size = res
		}
	}
	return size
}

func (d *DayFive) optimizePolymer(polymer string) int {
	index, err := d.createIndex(polymer)
	if err != nil {
		log.Fatal("Failed to index string")
	}
	curr := 1
	prev := 0

	for {
		pr, prHasValue := index.Get(prev)
		cr, crHasValue := index.Get(curr)
		if prHasValue && crHasValue {
			//log.Printf("Prev: i:%d - %s, Current swap case: j:%d - %s", prev, string(pr.(rune)), curr, string(swapCase(cr.(rune))))
			if pr.(rune) == swapCase(cr.(rune)) { // opposite polarity. Explotion
				index.Remove(curr) //Delete the one furthest down the list first
				index.Remove(prev) //Delete next one
				if curr >= 2 {
					curr--
				}
				if prev >= 1 {
					prev--
				}
				// d.index.Each(func(index int, value interface{}) {
				// 	log.Printf("i: %d - val: %s", index, string(value.(rune)))
				// })
			} else { //Iterate
				curr++
				prev++
			}
		} else {
			break
		}
	}
	//result := make([]rune, 0, d.index.Size()) //We assume a lot will be removed
	// it := d.index.Iterator()
	// for it.
	sb := strings.Builder{}
	sb.Grow(index.Size())
	// set.Each(func(index int, value interface{}) {
	// 	fmt.Print(value, " ")
	// })
	index.Each(func(index int, value interface{}) {
		r := value.(rune)
		_, err := sb.WriteRune(r)
		if err != nil {
			log.Fatal("Failed to insert rune in string builder")
		}
	})

	return len(sb.String())
}

//SETUP
func (d *DayFive) initPuzzle(test bool) {
	// cap := 50000
	// if test {
	// 	cap = 16
	// }
	//d.index = sll.New() //Singly Linked List

	b, err := ioutil.ReadFile(getFilePath(5, test)) // just pass the file name
	if err != nil {
		log.Fatal("Failed to read file")
	}

	d.raw = string(b) // convert content to a 'string'
	//_, err = d.createIndex(d.raw)
	if err != nil {
		log.Fatal(err) //Will exit OS
	}
}

func (d *DayFive) createIndex(line string) (*sll.List, error) {
	index := sll.New()
	for _, r := range line { //fill the linked list
		index.Add(r)
	}
	return index, nil
}
