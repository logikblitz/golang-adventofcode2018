package days

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"
)

type guard struct {
	id, slept int
	minutes   map[int]int
}

type DayFour struct {
	index  []record
	guards map[int]*guard
}

func (d DayFour) Answer(test bool) {
	d.initPuzzle(test)
	d.solveFirst()
	d.solveSecond()
}

func (d *DayFour) solveFirst() {
	log.Println("DayFour - Solve first puzzle")
	minute := d.processDayFirstPuzzlwe()
	log.Printf("DayFour - First Answer: %d\n", minute)
}

func (d *DayFour) solveSecond() {
	log.Println("DayFour - Solve first puzzle")
	value := d.processDaysecondPuzzle()
	log.Printf("DayFour - Second Answer: %d\n", value)
}

func (d *DayFour) processDaysecondPuzzle() int {
	guardID := 0
	max := 0
	minuteID := 0

	for _, guard := range d.guards {
		for minute, count := range guard.minutes {
			if count > max {
				guardID = guard.id
				max = count
				minuteID = minute
				//log.Printf("DayFour - count>max: c: %d, g: %d, m: %d, minId: %d\n", count, guardID, max, minuteID)
			}
		}
	}
	return guardID * minuteID
}

func (d *DayFour) processDayFirstPuzzlwe() int {
	//var currentGuard *guard
	currentguardID := 0
	d.guards = make(map[int]*guard, 0)
	var startSleep int
	for _, rec := range d.index {
		switch rec.text {
		case "falls asleep":
			//log.Println("Sleeps")
			startSleep = rec.minute
		case "wakes up":
			//log.Println("Wakes up")
			totalSleeptime := rec.minute - startSleep
			//log.Printf("Guard: %d, start %d, end %d, total %d\n", currentguardID, startSleep, rec.minute, totalSleeptime)
			d.guards[currentguardID].slept += totalSleeptime
			for i := startSleep; i < rec.minute; i++ {
				d.guards[currentguardID].minutes[i]++

			}
		default: //guard ID -> we always hit this first when processing a guard
			//log.Printf("ID time: %s\n", rec.text)
			guardID, err := strconv.Atoi(rec.text)
			if err != nil {
				log.Fatalf("Could not convert text to int: %s", rec.text) //Exit
			}
			g, exists := d.guards[guardID]
			if !exists { //Insert if not exist
				//log.Printf("Does not exist %s\n", rec.text)
				currentguardID = guardID
				d.guards[currentguardID] = &guard{ //Put the memory add in the map and manipulate that
					id:      currentguardID,
					minutes: make(map[int]int),
				}
			} else {
				currentguardID = g.id
			}
		}
	}

	for _, g := range d.guards { //Find the guard with the max sleep time
		//log.Printf("Guard: %v\n", *g)
		if g.slept > d.guards[currentguardID].slept {
			currentguardID = g.id
		}
	}
	max := 0
	selection := 0
	for minute, count := range d.guards[currentguardID].minutes {
		if count > max {
			max = count
			selection = minute
		}

	}
	return selection * currentguardID
}

//SETUP
func (d *DayFour) initPuzzle(test bool) {
	d.index = make([]record, 0)
	err := ReadLines(getFilePath(4, test), d.createIndex) //Insert date index
	if err != nil {
		log.Fatal(err)
	}
	//Sort the generated index
	//log.Printf("%v", d.index)
	sort.Sort(ByAge(d.index))
}

type record struct {
	year, month, day, hour, minute int
	text                           string
}

// ByAge implements sort.Interface for []record based on
// the time field.
type ByAge []record

func (a ByAge) Len() int           { return len(a) }
func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByAge) Less(i, j int) bool { return a[i].toTime().Unix() < a[j].toTime().Unix() }

func (r *record) toTime() time.Time {
	t := time.Date(r.year, time.Month(r.month), r.day, r.hour, r.minute, 0, 0, time.UTC)
	return t
}

func (d *DayFour) createIndex(line string) error {
	//[1518-11-01 23:58] Guard #99 begins shift
	//[1518-11-02 00:40] falls asleep
	//[1518-11-02 00:50] wakes up
	//var year, month, day, hour, minute int
	var maybeGuard, tmp string
	var rco record
	num, err := fmt.Sscanf(line, "[%d-%d-%d %d:%d] %s %s",
		//num, err := fmt.Sscanf(s, "#%d @ %d,%d: %dx%d",
		&rco.year, &rco.month, &rco.day, &rco.hour, &rco.minute, &maybeGuard, &tmp) // Must be reference to value
	if err != nil {
		return err
	}
	if num != 7 {
		log.Fatalf("invalid line actual %d expect 7", num)
	}
	if maybeGuard == "Guard" {
		rco.text = strings.Trim(tmp, "#")
	} else {
		rco.text = maybeGuard + " " + tmp
	}

	d.index = append(d.index, rco)
	//log.Printf("%d-%d-%d %d:%d %s %s\n", rco.year, rco.month, rco.day, rco.hour, rco.minute, rco.text)
	return nil
}
