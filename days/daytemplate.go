package days

import (
	"log"
)

type DayTemplate struct {
	index []string
}

func (d DayTemplate) Answer(test bool) {
	d.initPuzzle(test)
	d.solveFirst()
	d.solveSecond()
}

func (d *DayTemplate) solveFirst() {
	log.Println("DayTEMPLATE - Solve first puzzle")

	log.Printf("DayTEMPLATE - First Answer: %d\n", 100)
}

func (d *DayTemplate) solveSecond() {
	log.Println("DayTEMPLATE - Solve first puzzle")

	log.Printf("DayTEMPLATE - Second Answer: %d\n", 100)
}

//SETUP
func (d *DayTemplate) initPuzzle(test bool) {
	d.index = make([]string, 0)
	err := ReadLines(getFilePath(100, test), d.createIndex) //Insert date index
	if err != nil {
		log.Fatal(err) //Will exit OS
	}
}

func (d *DayTemplate) createIndex(line string) error {
	return nil
}
