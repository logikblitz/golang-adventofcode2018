package days

import "log"

type Adventpuzzle interface {
	Answer(test bool)
}

func getFilePath(day int, test bool) string {
	switch day {
	case 1:
		if test {
			return dayone_inputpathtest
		}
		return dayone_inputpath
	case 2:
		if test {
			return daytwo_inputpathtest
		}
		return daytwo_inputpath
	case 3:
		if test {
			return daythree_inputpathtest
		}
		return daythree_inputpath
	case 4:
		if test {
			return dayfour_inputpathtest
		}
		return dayfour_inputpath
	case 5:
		if test {
			return dayfive_inputpathtest
		}
		return dayfive_inputpath
	default:
		log.Fatalf("getFilePath - %d is not registred in the switch case statement to find the files for the days puzzle", day) // OS Exit
	}
	return ""
}

const dayone_inputpath string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayone_input.txt"
const dayone_inputpathtest string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayone_inputtest.txt"
const daytwo_inputpath string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/daytwo_input.txt"
const daytwo_inputpathtest string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/daytwo_inputtest2.txt"
const daythree_inputpath string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/daythree_input.txt"
const daythree_inputpathtest string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/daythree_inputtest.txt"
const dayfour_inputpath string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayfour_input.txt"
const dayfour_inputpathtest string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayfour_inputtest.txt"
const dayfive_inputpath string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayfive_input.txt"
const dayfive_inputpathtest string = "/Users/thomasblitz/Private/code/Programming/GOLang/src/gitlab.com/logikblitz/golang-adventofcode2018/days/dayfive_inputtest.txt"
