package days

import (
	"container/ring"
	"errors"
	"fmt"
	"strconv"

	"github.com/emirpasic/gods/sets/hashset"
)

type DayOne struct{}

var (
	index = make([]int, 0)
)

// func getFilePath(test bool) string {
// 	if test {
// 		return dayoneinputpathtest
// 	}
// 	return dayoneinputpath
// }

func (d DayOne) Answer(test bool) {
	initIndex(getFilePath(1, test))
	dayoneFirstRun()
	dayoneSecondRun()
}

func initIndex(path string) {
	err := ReadLines(path, createIndex)
	if err != nil {
		panic(err.Error)
	}
}

func dayoneFirstRun() {
	val := 0
	for _, i := range index {
		val += i
	}
	fmt.Printf("DayOne - First Answer: %d\n", val)
}

func dayoneSecondRun() {
	len := len(index)
	rng := ring.New(len)
	for _, val := range index {
		rng.Value = val
		rng = rng.Next()
	}
	val := 0

	set := hashset.New()

	for {
		//fmt.Printf("DayOne - ring value: %v\n", rng.Value)
		val += rng.Value.(int)
		if set.Contains(val) { //Stop criteria
			fmt.Printf("DayOne - Second Answer: %d\n", val)
			return
		}
		set.Add(val)
		rng = rng.Next()
	}
}

func createIndex(line string) error {
	val, err := strconv.Atoi(line)
	if err != nil {
		return errors.New("Text is not integer: " + line)
	}
	index = append(index, val)
	return nil
}
