package days

import (
	"fmt"
	"log"
)

type coord struct{ x, y int }

type claim struct {
	id            int
	left, top     int
	width, height int
}

type DayThree struct {
	claims   []claim
	claimMap map[coord][]int
}

func (d DayThree) Answer(test bool) {
	d.initPuzzle(test)
	d.solveFirst()
	d.solveSecond()
}

func (d *DayThree) solveFirst() {
	log.Println("DayThree - Solve first puzzle")
	squareInches := 0
	d.claimMap = d.createMapOfClaims(d.claims)
	for _, claimIds := range d.claimMap {
		if len(claimIds) > 1 { //If any coordinate has more than one registred ID then there is overlap
			squareInches++
		}
	}
	log.Printf("DayThree - First Answer: %d\n", squareInches)
}

func (d *DayThree) solveSecond() {
	log.Println("DayThree - Solve second puzzle")
OUTER:
	for _, c := range d.claims {
		for x := c.left; x < c.left+c.width; x++ { // Create x coordinates and walk
			for y := c.top; y < c.top+c.height; y++ { //Create y coordinates and walk
				if len(d.claimMap[coord{x, y}]) > 1 {
					continue OUTER // If a coordinate for the claim is shared/overlap then we go to the next claim
				}

			}
		}
		log.Printf("DayThree - Second Answer: %d\n", c.id)
	}
}

func (d *DayThree) createMapOfClaims(claims []claim) map[coord][]int {
	res := make(map[coord][]int)
	for _, c := range claims {
		for x := c.left; x < c.left+c.width; x++ { // Create x coordinates and walk
			for y := c.top; y < c.top+c.height; y++ { //Create y coordinates and walk
				res[coord{x, y}] = append(res[coord{x, y}], c.id) //Map coordinates to claim
			}
		}
	}
	return res
}

//SETUP
func (d *DayThree) initPuzzle(test bool) {
	d.claims = make([]claim, 0)
	err := ReadLines(getFilePath(3, test), d.createIndex)
	if err != nil {
		log.Fatal(err) // Will exit OS
	}
}

func (d *DayThree) createIndex(line string) error {
	claim, err := build(line)
	if err != nil {
		return err
	}
	d.claims = append(d.claims, claim)
	return nil
}

func build(s string) (claim, error) {
	//#10 @ 775,764: 22x28
	res := claim{}
	num, err := fmt.Sscanf(s, "#%d @ %d,%d: %dx%d",
		&res.id, &res.left, &res.top, &res.width, &res.height) // Must be reference to value
	if err != nil {
		return res, err
	}
	if num != 5 {
		log.Fatalf("invalid line actual %d expect 5", num)
	}
	return res, nil
}
