package days

import (
	"bufio"
	"log"
	"os"
	"strings"
	"unicode"
)

func ReadLines(path string, indexFunc func(line string) error) error {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		err := indexFunc(text)
		if err != nil {
			return err
		}
	}
	return nil
}

func StringGetSharedChars(first string, second string) []rune {
	runes := []rune(second)
	res := make([]rune, 0)
	for i, c := range first {
		if c == runes[i] {
			res = append(res, c)
		}
	}
	return res
}

func swapCase(rn rune) rune {
	if unicode.IsUpper(rn) {
		return unicode.ToLower(rn)
	}
	return unicode.ToUpper(rn)
}

func stripchars(str, chr string) string {
	return strings.Map(func(r rune) rune {
		if strings.IndexRune(chr, r) < 0 {
			return r
		}
		return -1
	}, str)
}
