package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/logikblitz/golang-adventofcode2018/days"
)

// var(
//     puzzles = []Adventpuzzle
// )

var puzzles []days.Adventpuzzle

func main() {
	test := flag.Bool("test", false, "Run testmode using predefined results: {true|false}")
	day := flag.Int("day", 0, "Thy day that should be solved. Valid range 0-25. For running thru all days use 0. Else use the number of the day -> 1-25")
	flag.Parse()
	InitLogger(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	hydratePuzzles()
	answerPuzzles(*day, *test)
}

func hydratePuzzles() {
	puzzles = []days.Adventpuzzle{
		days.DayOne{},
		days.DayTwo{},
		days.DayThree{},
		days.DayFour{},
		days.DayFive{},
	}
}

func answerPuzzles(day int, test bool) {
	index := day - 1 //convert to slice index
	if index <= -1 {
		fmt.Printf("Running ALL Puzzles!\n\n")
		for _, puzzle := range puzzles {
			puzzle.Answer(test)
		}
	} else if index >= len(puzzles) {
		message := fmt.Sprintf("Index out of range. Day # %d is not in range. The puzzles list is %d element long", day, len(puzzles))
		fmt.Println(message)
		panic(message)
	} else {
		fmt.Printf("Running Puzzle for the  %d. of December\n\n", day)
		puzzles[index].Answer(test)
	}
}

//Logging

var (
	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
)

func InitLogger(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}
